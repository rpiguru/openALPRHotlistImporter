import datetime
import os
import sys
import platform
import subprocess

from kivy.clock import Clock
from kivy.lang import Builder
from kivy.properties import ObjectProperty
import tempfile

from kivymd.time_picker import MDTimePicker
from screens.base.base_screen import BaseScreen
from settings import ROOT_DIR, LINUX_CRON_FILE
from utils.common import get_all_parsers, read_log, set_cron_job, logger, get_cron_setting
from widgets.dialog import FileChooserDialog, LogDialog, TestResultDialog, MsgDialog
from widgets.parser_item import ParserItem
from widgets.snackbar import show_info_snackbar


Builder.load_file('screens/main/main_screen.kv')


class MainScreen(BaseScreen):

    _file_chooser_dlg = ObjectProperty(allownone=True)
    _tp_dlg = ObjectProperty(allownone=True)
    _cur_parser = ObjectProperty()

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.parsers = get_all_parsers()

    def on_enter(self, *args):
        super(MainScreen, self).on_enter(*args)
        if platform.system() == 'Linux':
            self.ids.box_autorun.disabled = True
            cron_val = get_cron_setting()
            if cron_val:
                self.ids.txt_autorun_time.text = cron_val
        self.ids.txt_hotlist_loc.bind(focus=self._open_file_chooser_dlg)
        self.ids.txt_autorun_time.bind(focus=self._open_time_picker)
        self.ids.hotlist_parser.values = [p.get_parser_name() for p in self.parsers]
        self.ids.hotlist_parser.bind(on_changed=lambda w: self._populate_parsers())
        self._populate_parsers()

    def _open_file_chooser_dlg(self, *args):
        if self.ids.hotlist_loc_file.active and self._file_chooser_dlg is None and args[0].focus:
            self._file_chooser_dlg = FileChooserDialog(mode='file' )
            self._file_chooser_dlg.bind(on_confirm=self._on_file_selected)
            self._file_chooser_dlg.bind(on_dismiss=lambda d: self._on_file_chooser_closed())
            self._file_chooser_dlg.open()

    def _on_file_selected(self, *args):
        self.ids.txt_hotlist_loc.text = args[1]
        self.ids.txt_hotlist_loc.on_focus()
        self._file_chooser_dlg = None

    def _on_file_chooser_closed(self):
        self._file_chooser_dlg = None

    def _open_time_picker(self, *args):
        if self.ids.chk_autorun.active and self._tp_dlg is None and args[0].focus:
            self._tp_dlg = MDTimePicker()
            self._tp_dlg.bind(time=self._get_time_picker_data)
            self._tp_dlg.bind(on_dismiss=lambda d: self._on_tp_closed())
            if self.ids.txt_autorun_time.text:
                self._tp_dlg.set_time(datetime.datetime.strptime(self.ids.txt_autorun_time.text, '%H:%M:%S').time())
            self._tp_dlg.open()

    def _on_tp_closed(self):
        self._tp_dlg = None

    def _get_time_picker_data(self, inst, time):
        self.ids.txt_autorun_time.text = str(time)
        self.ids.txt_autorun_time.on_focus()
        self._tp_dlg = None

    def on_chk_autorun(self):
        if not self.ids.chk_autorun.active:
            self.ids.txt_autorun_time.on_focus()

    def _populate_parsers(self):
        new_parser_name = self.ids.hotlist_parser.get_value()
        self._cur_parser = self.parsers[self.ids.hotlist_parser.values.index(new_parser_name)]
        self.ids.txt_ex_format.text = self._cur_parser.get_example_format()
        self.ids.box.clear_widgets()
        self.ids.box.height = 0
        Clock.schedule_once(lambda dt: self.__add_a_parser_item(0))

    def __add_a_parser_item(self, index):
        lists = self._cur_parser.get_default_lists()
        if index < len(lists):
            item = ParserItem(name=lists[index]['name'], code=lists[index]['parse_code'])
            item.bind(on_remove=self._on_remove_item)
            item.bind(on_add=self._on_add_item)
            item.bind(on_changed=lambda w: self._set_to_custom_parser())
            self.ids.box.add_widget(item)
            self.ids.box.height += (item.height + 10)
            Clock.schedule_once(lambda dt: self.__add_a_parser_item(index + 1))
        else:
            Clock.schedule_once(lambda dt: self._on_finished_adding_parsers())

    def _on_finished_adding_parsers(self):
        if self.ids.box.children:
            self.ids.box.children[0].is_last = True
            self._show_blank_add_button(False)
        else:
            self._show_blank_add_button()
        self.ids.scroll.scroll_y = 1

    def _on_add_item(self, *args):
        box = self.ids.box
        if box.children:
            box.children[0].is_last = False
        item = ParserItem(is_last=True)
        item.bind(on_remove=self._on_remove_item)
        item.bind(on_add=self._on_add_item)
        item.bind(on_changed=lambda w: self._set_to_custom_parser())
        box.add_widget(item)
        box.height += (item.height + 10)
        self._set_to_custom_parser()

    def _on_remove_item(self, *args):
        box = self.ids.box
        box.remove_widget(args[0])
        box.height -= (args[0].height + 10)
        if box.children:
            box.children[0].is_last = True
        else:
            self._show_blank_add_button()
        self._set_to_custom_parser()

    def _set_to_custom_parser(self):
        lists = self._cur_parser.get_default_lists()
        is_changed = True
        if len(self.ids.box.children) == len(lists):
            is_changed = False
            for i, p in enumerate(lists):
                item = self.ids.box.children[::-1][i]
                if item.name != p['name'] or item.code != p['parse_code'] or item.is_override() or \
                        item.match_strategy != 'Exact':
                    is_changed = True
                    break
        if is_changed:
            self.ids.hotlist_parser.set_value("Custom")
        else:
            self.ids.hotlist_parser.set_value(self._cur_parser.get_parser_name())

    def _show_blank_add_button(self, val=True):
        self.ids.float_act_btn.disabled = not val
        self.ids.float_act_btn.opacity = 1 if val else 0

    def on_btn_blank(self):
        self._on_add_item()
        self._show_blank_add_button(False)

    def _on_blank_btn_released(self, *args):
        self._on_add_item()

    @staticmethod
    def on_btn_view_log():
        LogDialog(log=read_log()).open()

    def on_btn_test(self):
        if self.is_valid():
            conf_file = self.__generate_config_file(target_dir=tempfile.gettempdir())
            cmd = f"{sys.executable} openalpr_hotlist_import/hotlistimport.py {conf_file} --skip_upload --foreground"
            data = f'CMD: "{cmd}"\n{"=" * 60}\n'
            dlg = TestResultDialog(data=data, title="Test Result")
            dlg.auto_dismiss = False
            dlg.open()
            p = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True)
            Clock.schedule_once(lambda dt: self._process_test(p, dlg))
        else:
            MsgDialog(title='Error', message="Please input correct values").open()

    def _process_test(self, p, dlg):
        out = p.stdout.readline().decode()
        err = p.stderr.readline().decode()
        if p.poll() is not None:
            if out == '' and err == '':
                dlg.auto_dismiss = True
                logger.info('Finished Testing')
                return False
        if out != '':
            Clock.schedule_once(lambda dt: dlg.append_data(out))
        if err != '':
            Clock.schedule_once(lambda dt: dlg.append_data(err))
        Clock.schedule_once(lambda dt: self._process_test(p, dlg))

    def on_btn_save(self):
        if self.is_valid():
            conf_file = self.__generate_config_file(target_dir=ROOT_DIR)
            msg = f"Config file is saved as {conf_file}"
            if self.ids.chk_autorun.active:
                autorun_time = ":".join(self.ids.txt_autorun_time.text.split(':')[:2])
                set_cron_job(conf_file, autorun_time)
                msg += f' and will run at {autorun_time}'
            show_info_snackbar(msg, duration=5)
        else:
            MsgDialog(title='Error', message="Please input correct values").open()

    def __generate_config_file(self, target_dir):
        result_name = self._cur_parser.__class__.__module__.split('.')[-1]
        result_file = os.path.join(target_dir, f"{result_name}.yaml")

        data = self.collect_data()

        lines = [
            f"server_base_url: {data['server_url']}",
            f"company_id: {data['company_id']}",
            f"api_key: {data['api_key']}",
            f"hotlist_parser: {result_name}",
            f"hotlist_path: {data['hotlist_loc']}",
            f"temp_dat_file: {tempfile.gettempdir()}/hotlistimport.dat",
            f"temp_csv_file: {tempfile.gettempdir()}/hotlistimport.csv",
            f"log_file: {os.path.expanduser('~/.alpr/alpr_hotlist_importer.log')}",
            "log_archives: 5",
            "log_max_size_mb: 100",
        ]
        if 'state_import' in data:
            lines.append('"state_import:"')
            for s in data['state_import'].split(','):
                lines.append(f'  - {s}')
        if 'skip_plates' in data:
            lines.append('skip_list:')
            for s in data['skip_plates'].split(','):
                lines.append(f"  - '{s}'")
        lines.append('alert_types:')
        for w in self.ids.box.children:
            lines.append(f"  - name: {w.name}")
            lines.append(f"    parse_code: {w.code}")
            lines.append(f"    match_strategy: {w.match_strategy.lower()}")
            if w.is_override():
                lines.append(f"    hotlist_path: {w.get_hotlist_path()}")
        with open(result_file, 'w') as f:
            f.write('\n'.join(lines))

        return result_file
