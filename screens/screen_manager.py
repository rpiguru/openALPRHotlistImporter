from kivy.uix.screenmanager import ScreenManager

from screens.error.error_screen import ErrorScreen
from screens.main.main_screen import MainScreen


screens = {
    'main_screen': MainScreen,
    'error_screen': ErrorScreen,
}


sm = ScreenManager()
