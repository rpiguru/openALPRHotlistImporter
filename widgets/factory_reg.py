from kivy.factory import Factory

from kivymd.button import MDRaisedButton, MDFlatButton, MDIconButton
from kivymd.card import MDCard, MDSeparator
from kivymd.progressbar import MDProgressBar
from kivymd.selectioncontrols import MDCheckbox
from kivymd.spinner import MDSpinner
from kivymd.textfields import MDTextField
from widgets.checkbox import LabeledCheckbox
from widgets.input import ALPRMDTextField
from widgets.label import ScrollableLabel
from widgets.spinner import ALPRSpinner


Factory.register('MDCard', cls=MDCard)
Factory.register('MDRaisedButton', cls=MDRaisedButton)
Factory.register('MDFlatButton', cls=MDFlatButton)
Factory.register('MDIconButton', cls=MDIconButton)
Factory.register('MDSpinner', cls=MDSpinner)
Factory.register('MDTextField', cls=MDTextField)
Factory.register('MDCheckbox', cls=MDCheckbox)
Factory.register('MDSeparator', cls=MDSeparator)
Factory.register('MDProgressBar', cls=MDProgressBar)
Factory.register('ALPRSpinner', cls=ALPRSpinner)
Factory.register('ScrollableLabel', cls=ScrollableLabel)
Factory.register('ALPRMDTextField', cls=ALPRMDTextField)
Factory.register('LabeledCheckbox', cls=LabeledCheckbox)
