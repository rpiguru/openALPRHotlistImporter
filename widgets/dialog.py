import os

from kivy.lang import Builder
from kivy.properties import OptionProperty, StringProperty, ListProperty, NumericProperty
from kivy.uix.modalview import ModalView
from kivy.uix.popup import Popup

from kivymd.dialog import MDDialog
from kivymd.theming import ThemableBehavior
from widgets.snackbar import show_error_snackbar

Builder.load_file('widgets/kv/dialog.kv')


class FileChooserDialog(Popup):
    mode = OptionProperty('file', options=['file', 'dir'])
    extensions = ListProperty()
    path = StringProperty('')

    def __init__(self, **kwargs):
        super(FileChooserDialog, self).__init__(**kwargs)
        self.register_event_type('on_confirm')
        if self.path is None or not self.path:
            self.path = os.path.expanduser('~/Documents')

    def filter_content(self, directory, filename):
        if self.mode == 'file':
            if self.extensions:
                return filename.split('.')[-1] in self.extensions
            else:
                return True
        else:
            return os.path.isdir(os.path.join(directory, filename))

    def on_ok(self):
        if os.path.isfile(self.ids.txt_path.text) or self.mode == 'dir':
            self.dispatch('on_confirm', self.ids.txt_path.text)
            self.dismiss()
        else:
            show_error_snackbar(text='Please select file')

    def on_confirm(self, *args):
        pass

    def selected(self):
        self.ids.txt_path.text = str(self.ids.fc.selection[0])


class LoadingDialog(ModalView):
    pass


class ProgressDialog(ThemableBehavior, ModalView):

    title = StringProperty()
    value = NumericProperty()

    def update_progress(self, title="", value=0):
        self.title = title
        self.value = value


class LogDialog(MDDialog):

    log = StringProperty()

    def on_open(self):
        self.ids.lb_log.scroll_y = 0


class TestResultDialog(MDDialog):

    data = StringProperty()

    def on_open(self):
        self.ids.lb_data.scroll_y = 0

    def append_data(self, data):
        self.data += data
        self.ids.lb_data.text = self.data

    def set_auto_dismiss(self, val):
        self.auto_dismiss = val


class MsgDialog(MDDialog):
    message = StringProperty()


class InfoDialog(ModalView):

    text = StringProperty()
